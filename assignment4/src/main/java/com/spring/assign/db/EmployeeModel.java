package com.spring.assign.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.spring.assign.db.model.Employee;

public class EmployeeModel {

	private String insertString="insert into employee_details"+
	"(employee_id,employee_name,email,location) values(?,?,?,?)";
	
	private String getAllString="select * from employee_details";
	private String getOneEmployeeString=getAllString+" where employee_id = ?";
	public int addEmployee(Employee emp)
	{
		try {
		Connection con=DatabaseUtil.getConnection();
		PreparedStatement pSt=con.prepareStatement(insertString);
		pSt.setString(1, emp.getId());
		pSt.setString(2, emp.getName());
		pSt.setString(3, emp.getEmail());
		pSt.setString(4, emp.getLocation());
		return pSt.executeUpdate();
		}catch(Exception e) {
			return -1;
		}
	}
	public List<Employee>getEmployees()
	{
		try {
			Connection con=DatabaseUtil.getConnection();
			PreparedStatement pst=con.prepareStatement(getAllString);
			ResultSet rst=pst.executeQuery();
			List<Employee>list=new ArrayList<Employee>();
			while(rst.next())
			{
				
				Employee emp=new Employee(
						rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4)
						);
				list.add(emp);
			}
			return list;
		}catch(Exception e) {
			return null;
		}
	}
	
	public Employee getEmployee(String id)
	{
		try {
			Connection con=DatabaseUtil.getConnection();
			PreparedStatement pst=con.prepareStatement(getOneEmployeeString);
			pst.setString(1,id);
			Employee emp=null;
			ResultSet rst=pst.executeQuery();
			while(rst.next())
			{
			emp=new Employee(
						rst.getString(1),rst.getString(2),rst.getString(3),rst.getString(4)
						);
			}
			return emp;
		}
		catch(Exception e) {
			return null;
		}
	}
}
